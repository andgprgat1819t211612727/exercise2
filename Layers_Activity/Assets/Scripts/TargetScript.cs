﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{
    public int speed = 4;
    public static int testc = 5;
    public int position = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
    }

    void MovePlayer()
    {

        this.transform.position = new Vector2(this.transform.position.x + (speed * Time.deltaTime), this.transform.position.y);

    }
    public void SetValues(int sped)
    {
        this.speed = sped;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Wall"|| collision.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
        }
    }

}

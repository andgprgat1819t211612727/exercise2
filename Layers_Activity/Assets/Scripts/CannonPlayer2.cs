﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonPlayer2 : MonoBehaviour
{
    public GameObject bullet;
    public Transform bulletSpawn;
    public float caAngle = 0;
    public int power = 0;
    float smooth = 5;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.A))
        {
            caAngle += 5;
        }
        if (Input.GetKey(KeyCode.D))
        {
            caAngle += -5;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bullet, bulletSpawn.position, transform.rotation);
        }
        Quaternion target = Quaternion.Euler(0, 0, caAngle);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        //transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        //transform.Rotate(0, 0, 5 * Time.deltaTime);
        //PowerText.ammo = ammo;
    }
}

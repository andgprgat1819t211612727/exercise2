﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball1 : MonoBehaviour
{
    //public float angle = 45;
    //Vector velo;
    Vector3 velocity;
    Vector3 acceleration;
    //Vector gravity;
    public float xUnit;
    public float yUnit;
    public float mass;
    public Vector3 mForce;
    float x;
    float y;
    public float speed = 5;
    public float lastAngle;
    public float throwForce;
    public float posAngle;
    public float gravity;
    float xDegToRad;
    float yDegToRad;
    float xCoor;
    float yCoor;
    //GameObject ang = GameObject.FindGameObjectWithTag("Arrow");


    // Use this for initialization
    void Start()
    {

        GameObject ang = GameObject.FindGameObjectWithTag("Cannon1");
        CannonPlayer1 cannonAngle = ang.GetComponent<CannonPlayer1>();
        posAngle = cannonAngle.arAngle;
        throwForce = cannonAngle.power;
        //velo = new Vector(1, 1, 0);
        mForce = new Vector3(0, 10, 0);
        acceleration = new Vector3(0, 0, 0);
        velocity = new Vector3(this.transform.position.x, this.transform.position.y, 0);
        //gravity = new Vector(0, -19.8f, 0);
        xDegToRad = Mathf.Cos(Mathf.Deg2Rad * posAngle);
        yDegToRad = Mathf.Sin(Mathf.Deg2Rad * posAngle);
        xCoor = throwForce * xDegToRad;
        yCoor = throwForce * yDegToRad;

    }

    // Update is called once per frame
    void Update()
    {
        AddForce(xCoor, yCoor);
        UpdateMotion();
       // Debug.Log("Mass =" + mass);
        //Debug.Log("velox= " + velocity.x + "y= " + velocity.y);
        //Debug.Log("xCoor =" + xCoor + "yCoor =" + yCoor);
    }

    void ComputeVelocity()
    {
        //velo.x = Mathf.Cos(Mathf.Deg2Rad*angle);
        //velo.y = Mathf.Sin(Mathf.Deg2Rad*angle);
    }

    void AddForce(float x, float y)
    {
        this.acceleration.x += (x / mass);
        this.acceleration.y += (y / mass);
        //Debug.Log("Accel =" + acceleration.x + " y =" + acceleration.y);\
    }
    void UpdateMotion()
    {
        this.velocity.x += this.acceleration.x * Time.deltaTime;
        this.velocity.y += this.acceleration.y * Time.deltaTime;
        transform.position = new Vector2(this.velocity.x, this.velocity.y);
        this.acceleration.x *= 0;
        this.acceleration.y *= 0;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{

    public GameObject enemy;
    public GameObject spawnRight;
    public GameObject spawnLeft;
    GameObject enem;
    public bool isOkayToSpawn = false;
    public float spawnCountdow = 2.5f;

    // Use this for initialization
    void Start()
    {
        isOkayToSpawn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOkayToSpawn)
        {
            SpawnEnemy();
            spawnCountdow = 2.5f;
            isOkayToSpawn = false;
        }
        if(spawnCountdow<=0)
        {
            isOkayToSpawn = true;
        }
        spawnCountdow -= Time.deltaTime;
    }
    void SpawnEnemy()
    {
        float range = Random.Range(0, 100);
        if (range > 50)
        {
            GameObject ene = Instantiate(enemy, new Vector3(spawnLeft.transform.position.x, Random.Range(-2, 2), spawnLeft.transform.position.z), transform.rotation);
            TargetScript emen = ene.GetComponent<TargetScript>();
            emen.SetValues(2);
        }
        else if(range < 50)
        {
            GameObject ene = Instantiate(enemy, new Vector3(spawnRight.transform.position.x, Random.Range(-2, 2), spawnLeft.transform.position.z), transform.rotation);
            TargetScript emen = ene.GetComponent<TargetScript>();
            Debug.Log("Right Big " + ene.transform.position.x);
            emen.SetValues(-2);
        }

    }
}
